Add-Type @"
    using System;
    using System.Runtime.InteropServices;

    public class User32 {
        [DllImport("User32.dll")]
        public static extern IntPtr GetForegroundWindow();

        [DllImport("User32.dll")]
        public static extern int GetWindowThreadProcessId(IntPtr hWnd, out int lpdwProcessId);

        [DllImport("user32.dll")]
        public static extern bool AttachThreadInput(uint idAttach, uint idAttachTo, bool fAttach);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool BringWindowToTop(IntPtr hWnd);

        [DllImport("user32.dll")]
        public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        public static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, IntPtr dwExtraInfo);
    }
"@

function Force-ForegroundWindow {
    param(
        [IntPtr]$hWnd
    )

    $ALT_KEY = 0xA4
    $KEYEVENTF_KEYUP = 0x0002
    $SW_MAXIMIZE = 3

    [User32]::keybd_event($ALT_KEY, 0, 0, [IntPtr]::Zero) # Press ALT key down
    [User32]::ShowWindow($hWnd, $SW_MAXIMIZE) # Maximize window
    [User32]::SetForegroundWindow($hWnd)
    [User32]::keybd_event($ALT_KEY, 0, $KEYEVENTF_KEYUP, [IntPtr]::Zero) # Release ALT key
}

function BringAppToForeground {
    param(
        [string]$appName
    )

    $process = Get-Process -Name $appName -ErrorAction SilentlyContinue
    if ($process) {
        $process | ForEach-Object {
            Force-ForegroundWindow -hWnd $_.MainWindowHandle
        }
    } else {
        Write-Host "Process '$appName' not found. Starting a new instance."
        Start-Process $appName
    }
}

# Get the program name from command-line argument
$programName = $args[0]
if ($programName) {
    BringAppToForeground -appName $programName
} else {
    Write-Host "No program name specified."
}

exit # Close the command window
